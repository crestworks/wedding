@extends('main')

@section('title', 'Register | ')

@section('content')

<nav class="navbar navbar-expand-sm navbar-light">
  @include('partials._nav')
</nav>

<div class="container">

  <div class="row">
    <div class="col-md-12 flex-column align-items-center">
      <h2 class="heading-main mt-4">Email Password</h2>
      <hr class="heading-main-line">
    </div>
  </div>

  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="panel panel-default">

        <div class="panel-body">
          @if (session('status'))
          <div class="alert alert-success">
            {{ session('status') }}
          </div>
          @endif

          <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email" class="control-label">E-Mail Address</label>

              <div class="">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group">
              <div class="">
                <button type="submit" class="btn btn-block btn-primary">Send Password Reset Link</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
