@extends('main')

@section('title', 'Register | ')

@section('content')

<nav class="navbar navbar-expand-sm navbar-light">
  @include('partials._nav')
</nav>

<div class="container">

  <div class="row">
    <div class="col-md-12 flex-column align-items-center">
      <h2 class="heading-main mt-4">Register</h2>
      <hr class="heading-main-line">
    </div>
  </div>

  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="panel panel-default">
        <div class="panel-body">
          <form class="form-horizontal" method="POST" action="{{ route('register') }}">
            
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('first') ? ' has-error' : '' }}">
              <label for="first" class="control-label">First Name</label>

              <div class="">
                <input id="first" type="text" class="form-control" name="first" value="{{ old('first') }}" required autofocus>

                @if ($errors->has('first'))
                <span class="help-block">
                  <strong>{{ $errors->first('first') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('last') ? ' has-error' : '' }}">
              <label for="last" class="control-label">Last Name</label>

              <div class="">
                <input id="last" type="text" class="form-control" name="last" value="{{ old('last') }}" required autofocus>

                @if ($errors->has('last'))
                <span class="help-block">
                  <strong>{{ $errors->first('last') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email" class="control-label">E-Mail Address</label>

              <div class="">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label for="password" class="control-label">Password</label>

              <div class="">
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group">
              <label for="password-confirm" class="control-label">Confirm Password</label>

              <div class="">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
              </div>
            </div>

            <div class="form-group">
              <div class="">
                <button type="submit" class="btn btn-primary btn-block">Register</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
