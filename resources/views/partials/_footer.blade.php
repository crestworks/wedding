<footer class="container-fluid bg-light">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="social-icon-group mt-4 mb-2">
          <a href="https://www.facebook.com/groups/justmarriedjames/"><i class="fab fa-facebook-square"></i></a>
        </div>
        <p class="text-center">If you do not know either of these people you have probably landed on the wrong page. If you are considering crashing then our only request is that you know how to have a good time... and do not eat to much because we have not catered for you.</p>
        <p class="text-center mb-4">See you there!!</p>
      </div>
    </div>
  </div>
</footer>