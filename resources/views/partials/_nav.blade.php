
<div class="container flex-column-to-row">
  <a class="navbar-brand saffron-md" href="{{ route('home') }}">#Justmarriedjames</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse full-width" id="navbarSupportedContent">
    <ul class="navbar-nav full-width justify-content-between">
      <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">home</a>
      </li>
      <li class="nav-item {{ Request::is('wedding-party') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('wedding-party') }}">wedding party</a>
      </li>
      <li class="nav-item {{ Request::is('posts') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('posts') }}">posts</a>
      </li>
      <li class="nav-item {{ Request::is('gallery') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('gallery') }}">gallery</a>
      </li>
      <li class="nav-item {{ Request::is('contact') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('contact') }}">contact</a>
      </li>
    </ul>
  </div>
</div>