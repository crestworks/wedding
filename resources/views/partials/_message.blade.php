@if (Session::has('success'))
	
	<div class="alert alert-success" role="alert">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<strong>Success:</strong> {{ Session::get('success') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			</div>
		</div>
	</div>

@endif

@if (count($errors) > 0)

	<div class="alert alert-danger" role="alert">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<strong>Errors:</strong>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
					<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach  
					</ul>
				</div>
			</div>
		</div>
	</div>

@endif