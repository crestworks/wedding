@extends('main')

@section('title', 'Party | ')

@section('content')

<nav class="navbar navbar-expand-sm navbar-light">
  @include('partials._nav')
</nav>

<div class="container">
  <div class="row">
    <div class="col-md-12 flex-column align-items-center">
      <h2 class="heading-main mt-4">Femme Fatal</h2>
      <hr class="heading-main-line">
    </div>
    <div class="col-md-12 party-bg">
      <div class="card party-card">
        <div class="card-body d-flex flex-row-to-column justify-content-between">
          <div class="party-pic">
            <img src="{{ asset('party/1526229117.JPG') }}">
          </div>
          <div class="party-content">
            <h3 class="party-name">Charlotte Wetzlar</h3>
            <p class="party-title mb-0">The Bride</p>
            <hr class="party-break mt-1 mb-1">
            <p class="party-text mt-1 mb-0">I could not describe char in a paragraph or a book. She has so many incredible qualities and has been a role model for me. Her best quality by far is that she would move the world for the people she cares about. She knows no bounds in giving to others and has a huge heart, more so for animals. - <em>written by Emz</em></p>
          </div>
        </div>
      </div>
      <div class="card party-card">
        <div class="card-body d-flex flex-row-to-column justify-content-between">
          <div class="party-pic">
            <img src="{{ asset('party/1526229110.JPG') }}">
          </div>
          <div class="party-content">
            <h3 class="party-name">Sarah Symonds - "Seh"</h3>
            <p class="party-title mb-0">Maid of Honor</p>
            <hr class="party-break mt-1 mb-1">
            <p class="party-text mt-1 mb-0">Two years apart in age, Seh and Char have always been close. Sarah has been the voice of reason and conscience/angel on Chars shoulder over the years. Char has always counted on Seh for her honesty, and admired her for it.</p>
          </div>
        </div>
      </div>
      <div class="card party-card">
        <div class="card-body d-flex flex-row-to-column justify-content-between">
          <div class="party-pic">
            <img src="{{ asset('party/1526731446.JPG') }}">
          </div>
          <div class="party-content">
            <h3 class="party-name">Emily Wetzlar - "Emz"</h3>
            <p class="party-title mb-0">Bridesmaid</p>
            <hr class="party-break mt-1 mb-1">
            <p class="party-text mt-1 mb-0">Four years apart in age, Emz and Char were not as close earlier in life. Emz was Char’s dorky skinny little sister, whom Char and Pete fondly (and excessively) teased. Emz joined Char at uni in 2014, which is when they grew very close. Emz is Char’s adventure buddy and confidante.</p>
          </div>
        </div>
      </div>
      <div class="card party-card">
        <div class="card-body d-flex flex-row-to-column justify-content-between">
          <div class="party-pic">
            <img src="{{ asset('party/1526731448.JPG') }}">
          </div>
          <div class="party-content">
            <h3 class="party-name">Andrea Brown - "Onj"</h3>
            <p class="party-title mb-0">Bridesmaid</p>
            <hr class="party-break mt-1 mb-1">
            <p class="party-text mt-1 mb-0">Onj and Char have been friends since they were around 14 years old, and Onj has been Chars partner in crime ever since. Onj is that one friend that no matter how far apart they are and how long they have not seen each other Char knows she will always be there for her. </p>
          </div>
        </div>
      </div>
      <div class="card party-card">
        <div class="card-body d-flex justify-content-between flex-row-to-column">
          <div class="party-pic">
            <img src="{{ asset('party/1526731447.JPG') }}">
          </div>
          <div class="party-content">
            <h3 class="party-name">Dana James - "Dee"</h3>
            <p class="party-title mb-0">Bridesmaid</p>
            <hr class="party-break mt-1 mb-1">
            <p class="party-text mt-1 mb-0">Dee is the sister-to-be that has always really been Char’s sister - the wedding is just making it official. Dee has been there for Nick and Char both no matter what they were going through, and has been a true advocate for their relationship. </p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 flex-column align-items-center">
      <h2 class="heading-main mt-4">The Fellas</h2>
      <hr class="heading-main-line">
    </div>
    <div class="col-md-12 party-bg">
      <div class="card party-card">
        <div class="card-body d-flex flex-row-to-column justify-content-between">
          <div class="party-pic">
            <img src="{{ asset('party/1526229677.JPG') }}">
          </div>
          <div class="party-content">
            <h3 class="party-name">Nicholas James</h3>
            <p class="party-title mb-0">The Groom</p>
            <hr class="party-break mt-1 mb-1">
            <p class="party-text mt-1 mb-0">Nick & Charlotte met in their early teens and soon became an item. Having gone to school together (at St. Johns, an all boys school?!) and having similar sporting interests they were soon inseparable. An all out legend and true gentleman Nick currently spends his time keeping aeroplanes in  the air and Charlotte smitten. - <em>written by Jono</em></p>
          </div>
        </div>
      </div>
      <div class="card party-card">
        <div class="card-body d-flex flex-row-to-column justify-content-between">
          <div class="party-pic">
            <img src="{{ asset('party/1526730152.JPG') }}">
          </div>
          <div class="party-content">
            <h3 class="party-name">Jonothan Symonds - "Jono"</h3>
            <p class="party-title mb-0">Best Man</p>
            <hr class="party-break mt-1 mb-1">
            <p class="party-text mt-1 mb-0">Jono has always been a true friend and advisor to Nick. Jono and Nick met when Sarah started dating Jono. As Jono and Sarah’s romance grew, so did Jono and Nick’s bromance.</p>
          </div>
        </div>
      </div>
      <div class="card party-card">
        <div class="card-body d-flex flex-row-to-column justify-content-between">
          <div class="party-pic">
            <img src="{{ asset('party/1526730636.JPG') }}">
          </div>
          <div class="party-content">
            <h3 class="party-name">Daneric Hazelman - "Dan"</h3>
            <p class="party-title mb-0">Groomsman</p>
            <hr class="party-break mt-1 mb-1">
            <p class="party-text mt-1 mb-0">Daneric and Nick met during their Masters degrees. Daneric made the first move when he heard Nick’s accent, thought he was South African and was excited to talk to him about rugby. They became fast friends when they found even more common ground in their extremely weak humor.</p>
          </div>
        </div>
      </div>
      <div class="card party-card">
        <div class="card-body d-flex flex-row-to-column justify-content-between">
          <div class="party-pic">
            <img src="{{ asset('party/1526730354.JPG') }}">
          </div>
          <div class="party-content">
            <h3 class="party-name">Peter Wetzlar - "Pete"</h3>
            <p class="party-title mb-0">Groomsman</p>
            <hr class="party-break mt-1 mb-1">
            <p class="party-text mt-1 mb-0">Nick met Pete when he started dating Char, Pete was only ten. Nick used to pick Pete up from school, play catch with him and they bonded over the mutual desire for a brother. Back then, Pete looked up to Nick, now it is the other way round.. Literally. Like Dana has been a sister to Char, Pete has been a brother to Nick.</p>
          </div>
        </div>
      </div>
      <div class="card party-card">
        <div class="card-body d-flex justify-content-between flex-row-to-column">
          <div class="party-pic">
            <img src="{{ asset('party/1357680990.JPG') }}">
          </div>
          <div class="party-content">
            <h3 class="party-name">Boyd Littleford - "Boyd"</h3>
            <p class="party-title mb-0">Groomsman</p>
            <hr class="party-break mt-1 mb-1">
            <p class="party-text mt-1 mb-0">It was inevitable for Nick and Boyd to become friends as Boyd and Char have been close friends since forever. Boyd has been such a genuine and consistent friend to Nick and plays the part of both the angel and devil on Nicks shoulder.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop

@section('scripts')

<script type="text/javascript">
  $( ".party-card" ).hover(function() {
    var card = $( this ).find( ".card-body" );

    card.find( ".party-pic").addClass( "party-pic-animation");
  }, function() {
    var card = $( this ).find( ".card-body" );

    card.find( ".party-pic").removeClass( "party-pic-animation");
  });
</script>

@stop