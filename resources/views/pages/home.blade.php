@extends('main')

@section('title', 'Home | ')

@section('content')

<!-- <div class="container-fluid hero bg-light"> -->
<div class="container-fluid hero">

  <div class="hero-content">
    <!-- <div class="row"> -->
      <div class="hero-heading text-light">
        <h1 class="hero-heading-main saffron-xl">Nicholas James<br>&amp;<br>Charlotte Wetzlar</h1>
        <p class="hero-heading-sub">Saturday, August 4th 2018</p>
      </div>
    <!-- </div> -->
  </div>

  <nav class="navbar navbar-expand-sm navbar-dark">
    @include('partials._nav')
  </nav>

</div>
<div class="container">
  <div class="row">
    <div class="col-md-12 flex-column align-items-center">
      <h2 class="heading-main mt-4">The Couple</h2>
      <hr class="heading-main-line">
      <div class="couple-photo bg-light d-flex align-items-center justify-content-center mb-2">
        <!-- <i class="fas fa-image image-placeholder"></i> -->
        <img src="images/20045722_10213701370709230_4388198277049891912_o.jpg">
      </div>
      <p class="text-center">We first met at a waterpolo weekend at St. John's. I was 16 was trying to get a friend to get in an practice shooting (play five lives). He was chatting to a girl, who was with backup. This was when I first saw Char, she was this girls' backup. I thought she looked gorgeous and she thought "this guy does not stand a chance."
        <br>
      And then I smoothly got her number, and by that I mean my friend did and threatened that he would call her if I didn't.</p>
    </div>
  </div>
</div>
<div class="container mb-1">
  <div class="row">
    <div class="col-md-12 flex-column align-items-center">
      <h2 class="heading-main mt-2">Wedding Timeline</h2>
      <hr class="heading-main-line">
      <div class="itinerary">
        <div class="itinerary-line"></div>
        <div class="itinerary-event-container">
          <div class="itinerary-event mb-3">
            <div class="event-icon">
              <i class="fas fa-calendar"></i>
            </div>
            <div class="event">
              <div class="event-header d-flex">
                <div class="arrow-left"></div>
                <div class="event-title flex-grow-1">
                  <h3>Arrival</h3>
                  <hr>
                  <div class="event-date d-flex justify-content-between">
                    <p>2 Aug 2018</p>
                    <p>12:00pm</p>
                  </div>
                </div>
              </div>
              <div class="event-body-ml bg-light">
                <p>The wedding party, and a few other tag alongs will be arriving in Victoria Falls. This should leave enough time to do an activity in the afternoon.</p>
              </div>
            </div>
          </div>
          <div class="itinerary-event mb-3">
            <div class="event-icon">
              <i class="fas fa-calendar"></i>
            </div>
            <div class="event">
              <div class="event-header d-flex even">
                <div class="event-title flex-grow-1">
                  <h3>Boma</h3>
                  <hr>
                  <div class="event-date d-flex justify-content-between">
                    <p>3 Aug 2018</p>
                    <p>6:45pm</p>
                  </div>
                </div>
                <div class="arrow-right"></div>
              </div>
              <div class="event-body-mr bg-light">
                <p>The wedding party is going to the Boma (http://www.theboma.co.zw/). Everyone is welcome of course but you will have to make reservations. The evening will come to $40 per person. This is an amazing African Experience and not just a restaurant, this is will be an evening event with entertainment and food. We look forward to enjoying it with you.
                  <br>
                For more information and bookings please contact Louise Nielsen-Doran at louise@vicfallsoccasions.com.</p>
              </div>
            </div>
          </div>
          <div class="itinerary-event mb-3">
            <div class="event-icon">
              <i class="fas fa-calendar"></i>
            </div>
            <div class="event">
              <div class="event-header d-flex">
                <div class="arrow-left"></div>
                <div class="event-title flex-grow-1">
                  <h3>Pickup</h3>
                  <hr>
                  <div class="event-date d-flex justify-content-between">
                    <p>4 Aug 2018</p>
                    <p>3:00pm</p>
                  </div>
                </div>
              </div>
              <div class="event-body-ml bg-light">
                <p>Everyone must be ready and at any of the collection points. The shuttles will be collecting everyone from the designated points and taking them to the boarding location.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop