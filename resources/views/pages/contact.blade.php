@extends('main')

@section('title', 'Contact | ')

@section('content')

<nav class="navbar navbar-expand-sm navbar-light">
  @include('partials._nav')
</nav>

<div class="container">
  <div class="row">
    <div class="col-md-12 flex-column align-items-center">
      <h2 class="heading-main mt-4">Contact Us</h2>
      <hr class="heading-main-line">
    </div>
  </div>

  <div class="row">
    <div class="col-md-6 offset-md-3">
      <form action="{{ route('contact-email') }}" method="POST">

        {{ csrf_field() }}

        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" id="name" name="name" placeholder="Hank Frankle">
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" name="email" placeholder="hankfrankle@gmail.com">
        </div>
        <div class="form-group">
          <label for="send">Send To</label>
          <select class="form-control" id="send" name="send">
            <option>Select a contact</option>
            <option value="planner">Louise (Wedding Planner)</option>
            <option value="couple">Bride & Groom</option>
          </select>
        </div>
        <div class="form-group">
          <label for="subject">Subject</label>
          <input type="text" class="form-control" id="subject" name="subject" placeholder="Cruise Bording">
        </div>
        <div class="form-group">
          <label for="body">Message</label>
          <textarea type="text" class="form-control" id="body" name="body" placeholder="Write your message here..." rows="6"></textarea>
        </div>
        <button type="submit" class="btn btn-block btn-primary">Send</button>
      </form>
    </div>
  </div>
</div> 

@stop