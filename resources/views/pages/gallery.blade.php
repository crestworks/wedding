@extends('main')

@section('title', 'Gallery | ')

@section('content')

<nav class="navbar navbar-expand-sm navbar-light">
  @include('partials._nav')
</nav>

<div class="container">
  <div class="row">
    <div class="col-md-12 flex-column align-items-center">
      <h2 class="heading-main mt-4">Wedding Gallery</h2>
      <hr class="heading-main-line">
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 d-flex flex-wrap">
      <div class="gallery gallery-odd">
        <div class="landscape mb-4">
          <img src="{{ asset('images/312561_180705648738103_1739912149_n.jpg') }}">
        </div>
        <div class="portrait mb-4">
          <img src="{{ asset('images/1397522_323333617808638_1176272274_o.jpg') }}">
        </div>
      </div>
      <div class="gallery gallery-even">
        <div class="landscape mb-4">
          <img src="{{ asset('images/403200_10150488416566172_346402751_n.jpg') }}">
        </div>
        <div class="portrait mb-4">
          <img src="{{ asset('images/1503350_10204321209643070_123531993139428340_n.jpg') }}">
        </div>
      </div>
      <div class="gallery gallery-odd">
        <div class="landscape mb-4">
          <img src="{{ asset('images/10387144_10204372583695385_5032177079772908612_o.jpg') }}">
          </div>
        <div class="portrait mb-4">
          <img src="{{ asset('images/10454404_437097933098872_5923583622244835076_o.jpg') }}">
        </div>
      </div>
      <div class="gallery gallery-even">
        <div class="landscape mb-4">
          <img src="{{ asset('images/10721344_10205007531208676_1702847600_n.jpg') }}">
        </div>
        <div class="portrait mb-4">
          <img src="{{ asset('images/12063898_651514568323873_4827292267207758958_n.jpg') }}">
        </div>
      </div>
      <div class="gallery gallery-odd">
        <div class="landscape mb-4">
          <img src="{{ asset('images/11036368_607990029342994_8473394632327648370_n.jpg') }}">
        </div>
        <div class="portrait mb-4">
          <img src="{{ asset('images/17211816_10154254579661837_7103240609064885947_o.jpg') }}">
        </div>
      </div>
    </div>
  </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
  var images = $('img').length
  var gallery = $('.gallery').length
  var galleryOdd = $('.gallery-odd').length
  var galleryEven = $('.gallery-even').length
  var landscape = $('.landscape').length
  var portrait = $('.portrait').length
  var multiple = Math.floor(images / 6)
  var remainder = images - (multiple * 6)

  console.log(images)
  console.log(gallery)
  console.log(galleryOdd)
  console.log(galleryEven)
  console.log(landscape)
  console.log(portrait)
  console.log(multiple)
  console.log(remainder)

  

</script>
@stop