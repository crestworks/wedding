@extends('main')

@section('title', 'Locations & Activities | ')

@section('content')

<nav class="navbar navbar-expand-sm navbar-light">
  @include('partials._nav')
</nav>

<div class="container">
  <div class="row">
    <div class="col-md-12 flex-column align-items-center">
      <h2 class="heading-main mt-4">Map</h2>
      <hr class="heading-main-line">
    </div>
  </div>
</div>

<div calss="container-fluid">
  <div class="map">
    <div id="map"></div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-12 flex-column align-items-center">
      <h2 class="heading-main mt-4">Activities</h2>
      <hr class="heading-main-line">
      <p class="text-center">There are some things you have to do while in Victoria Falls and some things that would be nice to do. The wedding is must do just like visiting the Falls. The Boma, sunset cruise, bungee jumping, gorge swing, white water rafting are some other great options. Please use the contact form below to find out more details.</p>
      <p class="text-center">NOTE: All prices below are in US Dollars and are per person rates.</p>
      <div class="d-flex flex-column mb-3">
        <div class="d-flex mb-1">
          <p class="mb-0">White Water Rafting</p>
          <div class="tab-dots"></div>
          <p class="mb-0">120</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Upper Zambezi Canoeing</p>
          <div class="tab-dots"></div>
          <p class="mb-0">150</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Gorge Swing</p>
          <div class="tab-dots"></div>
          <p class="mb-0">95</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Zip Line</p>
          <div class="tab-dots"></div>
          <p class="mb-0">69</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Flying Fox</p>
          <div class="tab-dots"></div>
          <p class="mb-0">42</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Canopy Tour</p>
          <div class="tab-dots"></div>
          <p class="mb-0">53</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Chobe Trip (full day)</p>
          <div class="tab-dots"></div>
          <p class="mb-0">160</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Sundowner Cruise</p>
          <div class="tab-dots"></div>
          <p class="mb-0">40</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Sundowner Cruise (luxury)</p>
          <div class="tab-dots"></div>
          <p class="mb-0">70</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Elephant Encounter</p>
          <div class="tab-dots"></div>
          <p class="mb-0">75</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Helicopter (13min)</p>
          <div class="tab-dots"></div>
          <p class="mb-0">150</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Helicopter (15min)</p>
          <div class="tab-dots"></div>
          <p class="mb-0">179</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Game Drive</p>
          <div class="tab-dots"></div>
          <p class="mb-0">100</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Fishing (3 hours)</p>
          <div class="tab-dots"></div>
          <p class="mb-0">100</p>
        </div>
        <div class="d-flex mb-1">
          <p class="mb-0">Croc Cage Diving</p>
          <div class="tab-dots"></div>
          <p class="mb-0">70</p>
        </div>
      </div>
    </div>
  </div>
</div>

@stop

@section('scripts')

<!-- GOOGLE MAPS -->
<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpUUSWloS2znEF9-vxePF5vqNE-p2fCFE&amp;callback=googleMap"></script>

<script type="text/javascript">
function googleMap() {

  var mapColors = [
    {
      "featureType": "landscape.natural",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#ebece7"
        }
      ]
    },
    {
      "featureType": "poi.business",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#e6e3db"
        }
      ]
    },
    {
      "featureType": "poi.business",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#a9a19f"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#e6e3db"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#a9a19f"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#ddcec4"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#a9a19f"
        }
      ]
    }
  ];

  var location = {lat: -17.901381, lng: 25.828455};

  var center = {lat: -17.919511, lng: 25.836437};
  var map;
  var marker;

  function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 14,
      center: center,
      styles: mapColors
    });

    marker = new google.maps.Marker({
      position: location,
      map: map,
      title: "Wedding Venue"
    });
  }

  initMap();

  var latLng = new google.maps.LatLng(center.lat, center.lng);
  map.panTo(latLng);
  marker.setPosition( new google.maps.LatLng(location.lat, location.lng));
}

</script>

@stop