@extends('main')

@section('title', 'Posts | ')

@section('content')

<nav class="navbar navbar-expand-sm navbar-light">
  @include('partials._nav')
</nav>

<div class="container">
  <div class="row">
    <div class="col-md-12 flex-column align-items-center">
      <h2 class="heading-main mt-4">Wedding Posts</h2>
      <hr class="heading-main-line">
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="post-card">
        <div class="post-nav-header bg-light d-flex justify-content-between align-items-baseline">
          @if (Auth::guest())
            <p class="mb-1 mt-1">Login to post <span class="post-nav-header-extra-text">or give us some advice</p>
          @else
            <p class="mb-1 mt-1">Write a post <span class="post-nav-header-extra-text">or give us some advice</p>
          @endif

          @if (Auth::guest())
            <a href="{{ route('login') }}" class="card-link">Sign In</a>
          @else

          <div class="dropdown">
            <a class="card-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ substr(Auth::user()->first, 0, 1). ". " . Auth::user()->last }}</a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item" href="{{ route('profile') }}">Update Profile</a>
              <form action="{{ route('logout') }}" method="POST">
                {{ csrf_field() }}
                <button class="dropdown-item" href="{{ route('logout') }}">Logout</button>
              </form>
              
            </div>
          </div>

          @endif
          
        </div>
        @if (Auth::check())
        <div class="d-flex post-input">
          <div class="post-pic-default saffron-md bg-primary">
            <p class="text-light">{{ substr(Auth::user()->first, 0, 1) }}</p>
            <p class="text-light">{{ substr(Auth::user()->last, 0, 1) }}</p>
          </div>
          <div class="posts-content">
            <form action="{{ route('posts.store') }}" method="POST">

              {{ csrf_field() }}

              <div class="form-group">
                <textarea name="post" class="form-control" placeholder="Write your message here..."></textarea>
              </div>
              <button type="submit" class="btn btn-primary btn-block btn-sm">Post</button>
            </form>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>

  @foreach($posts as $post)
  <div class="row">
    <div class="col-md-12">
      <div class="card posts-card">
        <div class="card-body d-flex">
          <div class="post-pic-default saffron-md bg-primary">
            <p class="text-light">{{ substr($post->user->first, 0, 1) }}</p>
            <p class="text-light">{{ substr($post->user->last, 0, 1) }}</p>
          </div>
          <div class="posts-content">
            <h4 class="posts-name saffron-md">{{ $post->user->first }} {{ $post->user->last}}</h4>
            <div class="d-flex justify-content-between">
              <p class="posts-date mb-0">{{ date('M j, Y H:i', strtotime($post->created_at)) }}</p>
              <p class="posts-date mb-0">Comments {{ $post->comments()->count() }}</p>
            </div>
            <hr class="posts-break mt-1 mb-1">
            <p class="post-id" hidden>{{ $post->id }}</p>
            <p class="posts-text mt-1 mb-0">{{ $post->post }}
              @if($post->user_id == $user_id)
              &nbsp;<a class="card-link"
                    href="#"
                    data-toggle="modal"
                    data-target="#postModal"
                    data-id="{{ $post->id }}"
                    data-post="{{ $post->post }}">(edit)</a>
              @endif
            </p>
            <hr class="posts-break mt-1 mb-3">

            @foreach($post->comments as $comment)
            <div class="d-flex mb-3">
              <div class="comment-pic-default saffron-md bg-primary">
                <p class="text-light">{{ substr($comment->user->first, 0, 1) }}</p>
                <p class="text-light">{{ substr($comment->user->last, 0, 1) }}</p>
              </div>
              <div class="comment-container">
                <div class="d-flex align-items-baseline justify-content-between">
                  <h5 class="comments-name saffron-sm">{{ $comment->user->first }}<span> {{ $comment->user->last}}</span></h5>
                  <p class="posts-date mb-0">{{ date('M j, Y H:i', strtotime($comment->created_at)) }}</p>
                </div>
                <p class="comments-text mb-0">{{ $comment->comment }}
                  @if($comment->user_id == $user_id)
                  &nbsp;<a class="card-link"
                    href="#"
                    data-toggle="modal"
                    data-target="#commentModal"
                    data-id="{{ $comment->id }}"
                    data-post="{{ $comment->post_id }}"
                    data-comment="{{ $comment->comment }}">(edit)</a>
                  @endif
                </p>
              </div>
            </div>
            @endforeach

            @if (Auth::check())
            <div class="d-flex mb-3">
              <div class="comment-pic-default saffron-md bg-primary">
                <p class="text-light">{{ substr(Auth::user()->first, 0, 1) }}</p>
                <p class="text-light">{{ substr(Auth::user()->last, 0, 1) }}</p>
              </div>
              <div class="comment-container">
                <form action="{{ route('comments.store') }}" method="POST">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <input type="text" name="post_id" value="{{ $post->id }}" hidden>
                    <textarea name="comment" class="form-control" placeholder="Write your comment here"></textarea>
                  </div>
                  <button type="submit" class="btn btn-primary btn-block btn-sm">Comment</button>
                </form>
              </div>
            </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach

<!-- Modal -->
<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form class="" action="{{ route('comments.store') }}" method="POST">

        {{ method_field('PUT') }}
        {{ csrf_field() }}

        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Your Comment</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="text" class="commentId" name="comment_id" hidden>
          <input type="text" class="postId" name="post_id" hidden>
          <textarea name="comment" class="form-control" placeholder="Write your comment here"></textarea>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" id="delete-comment" class="btn btn-outline-danger align-self-start">Delete</button>
            <div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </form>

      <form class="alert alert-danger message-delete" id="delete-comment-dropdown" action="{{ route('comments.destroy') }}" method="POST">

        {{ method_field('DELETE') }}
        {{ csrf_field() }}

        <p><strong>You are about to delete this Comment. Are you sure you want to go ahead with this?</strong></p>
        <input type="text" class="commentId" name="comment_id" hidden>
        <button type="submit" class="btn btn-danger align-self-start">Confirm</button>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="postModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="{{ route('posts.update') }}" method="POST">

        {{ method_field('PUT') }}
        {{ csrf_field() }}

        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Your Post</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="text" name="post_id" hidden>
          <textarea name="post" class="form-control" placeholder="Write your post here"></textarea>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" id="delete-post" class="btn btn-outline-danger align-self-start">Delete</button>
          <div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Changes</button>
          </div>
        </div>
      </form>

      <form class="alert alert-danger message-delete" id="delete-post-dropdown" action="{{ route('posts.destroy') }}" method="POST">

        {{ method_field('DELETE') }}
        {{ csrf_field() }}

        <p><strong>You are about to delete this Post and all associated data. Are you sure you want to go ahead with this?</strong></p>
        <input type="text" id="post-delete-input" name="post_id" hidden>
        <button type="submit" class="btn btn-danger align-self-start">Confirm</button>
      </form>
    </div>
  </div>
</div>

</div>

@stop

@section('scripts')
<script type="text/javascript">
  $('#commentModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var commentId = button.data('id')
    var postId = button.data('post')
    var commentText = button.data('comment') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.commentId').val(commentId)
    modal.find('.postId').val(postId)
    modal.find('.modal-body textarea').val(commentText)
  });

  $('#postModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var postId = button.data('id')
    var postText = button.data('post') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-body input').val(postId)
    modal.find('#post-delete-input').val(postId)
    modal.find('.modal-body textarea').val(postText)
  });

  $("#delete-post").click(function(){
    $("#delete-post-dropdown").slideToggle();
  });

  $("#delete-comment").click(function(){
    $("#delete-comment-dropdown").slideToggle();
  });

</script>
@stop