<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// POSTS
// Route::resource('posts', 'PostsController');
Route::post('posts', 'PostsController@storePost')->name('posts.store');
Route::put('posts', 'PostsController@updatePost')->name('posts.update');
Route::delete('posts', 'PostsController@destroyPost')->name('posts.destroy');

// COMMENTS
// Route::resource('comments', 'CommentsController');
Route::post('comments', 'CommentsController@storeComment')->name('comments.store');
Route::put('comments', 'CommentsController@updateComment')->name('comments.update');
Route::delete('comments', 'CommentsController@destroyComment')->name('comments.destroy');

// PAGES
Route::get('wedding-party', 'PagesController@getWeddingParty')->name('wedding-party');
Route::get('posts', 'PagesController@getPosts')->name('posts');
Route::get('gallery', 'PagesController@getGallery')->name('gallery');
Route::get('locations', 'PagesController@getLocations')->name('locations');

// CONTACT
Route::get('contact', 'PagesController@getContact')->name('contact');
Route::post('contact', 'PagesController@postContact')->name('contact-email');

// PROFILE
Route::get('profile', 'PagesController@getProfile')->name('profile');
Route::put('profile', 'PagesController@updateProfile')->name('update-profile');

Route::get('/', 'PagesController@getHome')->name('home');


Auth::routes();
