<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;

use App\Post;
use App\Comment;

class PostsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePost(Request $request)
    {
        // validate data
        $this->validate($request, array(
            'post' => 'required|min:5|max:2000'
        ));

        // store in the database
        $user = Auth::user()->id;

        $post = new Post;

        $post->post = $request->post;
        $post->user_id = $user;

        $post->save();

        Session::flash('success', 'You successfully posted your message!');

        // redirect
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePost(Request $request)
    {
        // find in database
      $post = Post::find($request->post_id);

      // validate data
      $this->validate($request, array(
          'post' => 'required|min:5|max:2000'
      ));

      $user = Auth::user()->id;

      // store in the database
      $post->post = $request->post;
      $post->user_id = $user;

      $post->save();

      Session::flash('success', 'Your Post was successfully edited!');

      // redirect
      return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyPost(Request $request)
    {
        $post = Post::find($request->post_id);

        $post->delete();

        Session::flash('success', 'Your Post and all associated Comments has been deleted');

        // redirect
        return redirect()->back();
    }
}
