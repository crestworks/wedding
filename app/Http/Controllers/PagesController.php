<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use Mail;
use Auth;
use App\Post;
use App\User;
use App\Comment;

class PagesController extends Controller
{
  public function getHome()
	{

		return view('pages.home');

	}

	public function getWeddingParty()
	{

		return view('pages.wedding-party');

	}

	public function getPosts()
	{
		$posts = Post::all()->sortByDesc('created_at');

		$user_id = 0;

		if(Auth::check()) {
			$user_id = Auth::user()->id;
		}

		return view('pages.posts')->withPosts($posts)->withUser_id($user_id);

	}

	public function getGallery()
	{

		return view('pages.gallery');

	}

	public function getContact()
	{

		return view('pages.contact');

	}

	public function postContact(Request $request)
	{
		$this->validate($request, array(
			'name' => 'required',
			'email' => 'required|email',
			'send' => 'required',
			'subject' => 'required|min:3',
			'body' => 'required|min:10'
		));

		$send = 'example@email.com';

		if($request->send == 'planner') {
			$send = 'louise@vicfallsoccasions.com';
			// $send = 'nj167252@reddies.hsu.edu'; testing
		} else if ($request->send == 'couple') {
			$send = 'charlottewetzlar@gmail.com';
			// $send = 'nick@haczim.com'; testing
		}

		$data = array(
			'name' => $request->name,
			'email' => $request->email,
			'send' => $send,
			'subject' => $request->subject,
			'body' => $request->body,
		);

		Mail::send('emails.contact', $data, function($message) use ($data)
		{
			$message->from($data['email']);
			$message->to($data['send']);
			$message->subject($data['subject']);
		});

		Session::flash('success', 'Your Message was successfully sent!');

      // redirect
      return redirect()->back();
	}

	public function getLocations()
	{

		return view('pages.locations');

	}

	public function getProfile()
	{
		$user = Auth::user();

		return view('pages.profile')->withUser($user);

	}

	public function updateProfile(Request $request)
	{
		$id = Auth::user()->id;

		// Validate the data
    $user = User::find($id);
    if ($request->input('email') == $user->email) {
      $this->validate($request, array(
        'first' => 'required|string|max:255',
        'last' => 'required|string|max:255'
      ));
    } else {
    	$this->validate($request, array(
        'first' => 'required|string|max:255',
            'last' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
      ));
    }

    // Save the data to the database
    $user = User::find($id);
    $user->first = $request->input('first');
    $user->last = $request->input('last');
    $user->email = $request->input('email');
    
    $user->save();

    // set flash data with success message
    Session::flash('success', 'Your Profile was successfully updated.');

    // redirect with flash data to posts.show
    return redirect()->route('posts');
	}
}
