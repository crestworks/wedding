<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;

use App\Post;
use App\Comment;

class CommentsController extends Controller
{
	/**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function storeComment(Request $request)
  {
      // validate data
      $this->validate($request, array(
          'comment' => 'required|min:5|max:2000'
      ));

      // store in the database
      $user = Auth::user()->id;

      $comment = new Comment;

      $comment->comment = $request->comment;
      $comment->user_id = $user;
      $comment->post_id = $request->post_id;

      $comment->save();

      Session::flash('success', 'You successfully commented on a message!');

      // redirect
      return redirect()->back();
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function updateComment(Request $request)
  {
  	// find in database
    $comment = Comment::find($request->comment_id);

    // validate data
    $this->validate($request, array(
        'comment' => 'required|min:5|max:2000'
    ));

    $user = Auth::user()->id;

    // store in the database
    $comment->comment = $request->comment;
    $comment->user_id = $user;
    $comment->post_id = $request->post_id;

    $comment->update();

    Session::flash('success', 'Your Comment was successfully edited!');

    // redirect
    return redirect()->back();

  }

  /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyComment(Request $request)
    {
        $post = Comment::find($request->comment_id);

        $post->delete();

        Session::flash('success', 'Your Comment has been deleted');

        // redirect
        return redirect()->back();
    }
}
