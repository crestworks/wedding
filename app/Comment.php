<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	/**
   * Get the User that owns the comment.
   */

  public function user()
  {
      return $this->belongsTo('App\User');
  }

  /**
   * Get the Post that owns the comment.
   */

  public function post()
  {
      return $this->belongsTo('App\Post');
  }
}
